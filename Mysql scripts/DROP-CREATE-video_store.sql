/*************************************************
*  Author:           Johannes Holtzhausen
*  Date:             2023-10-23
*  Filename:         DROP-CREATE-video_store.sql
*  Description:
*
* Creates the video_store database and members
**************************************************/

/* Dropping and recreating the database */
DROP DATABASE IF EXISTS video_store;
CREATE DATABASE video_store;
USE video_store;


CREATE TABLE customers(
	custId INT PRIMARY KEY AUTO_INCREMENT,
    fname VARCHAR(40) NOT NULL,
    sname VARCHAR(40) NOT NULL,
    address VARCHAR(40) NOT NULL,
    phone VARCHAR(10) NOT NULL UNIQUE
)ENGINE = InnoDB;

CREATE TABLE videos(
	videoId INT NOT NULL AUTO_INCREMENT,
    videoVer INT NOT NULL,
    vname VARCHAR(15) NOT NULL,
    `type` VARCHAR(1) NOT NULL,
    dateAdded DATE NOT NULL,
    PRIMARY KEY (videoId)
)ENGINE = InnoDB;

CREATE TABLE hire(
	custId INT NOT NULL,
    videoId INT NOT NULL,
    -- videoVer INT NOT NULL, -- There is no explination of what this NOT NULL column should do?????
    dateHired DATE NOT NULL,
    dateReturn DATE,
    PRIMARY KEY (custId, videoId),
    FOREIGN KEY (custId)
    REFERENCES customers(custId),
    FOREIGN KEY (videoId)
    REFERENCES videos(videoId)
)ENGINE = InnoDB;


DELIMITER $$
CREATE PROCEDURE sp_add_movie(IN vname_var VARCHAR(50), IN type_var VARCHAR(1), IN dateAdded_var DATE)
BEGIN
	DECLARE id_var INT DEFAULT 0;
    
	IF EXISTS (SELECT videoId FROM videos WHERE vname = vname_var) THEN
        SELECT videoId INTO id_var FROM videos WHERE vname = vname_var;
		
        UPDATE videos
        SET videoVer = videoVer + 1
        WHERE videoId = id_var;
	ELSE 
		INSERT INTO videos(vname, `type`, dateAdded, videoVer)
		VALUES (vname_var, type_var, dateAdded_var, 1);
	END IF;
END$$
DELIMITER ;

















