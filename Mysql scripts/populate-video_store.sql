/*************************************************
 *  Author:           Johannes Holtzhausen
 *  Date:             2023-10-30
 *  Filename:         populate-video_store.sql
 *  Description:
 *
 * Populate's the members of the video_store database.
 **************************************************/
USE video_store;
INSERT INTO customers(fname, sname, address, phone)
VALUES (
        'Jhaan',
        'Holtzhausen',
        '252 Marine road',
        '0726842965'
    ),
    (
        'Piet',
        'Pompies',
        '18 Soetdoring street',
        '0826750945'
    ),
    (
        'Sannie',
        'Laanie',
        '50 Merister street',
        '0658675642'
    );
CALL sp_add_movie('the nun ii', 'r', '2023-10-29');
CALL sp_add_movie('max', 'r', '2023-10-30');
CALL sp_add_movie('alien 3', 'b', '2023-10-30');
CALL sp_add_movie('star wars', 'b', '2023-10-30');
INSERT INTO hire(custId, videoId, dateHired)
VALUES (1, 1, '2023-10-29'),
    (2, 1, '2023-10-30'),
    (3, 3, '2023-10-30');