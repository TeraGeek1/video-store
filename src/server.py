## server.py
import sys
import threading
import socket as soc
import json
import time

try:
    import mysql.connector
except ImportError as ex:
    print(ex)
    print("\n\n\n")
    
    missing_package = ex.args[0]
    
    if missing_package == "No module named 'mysql'":
        print("Failed to import the mysql connector")

    print("Make shore that you installed all the dependencies in the \"requirements.txt\" file.")
    input("Press enter to close")
    sys.exit()



get_date = lambda: time.strftime("%Y-%m-%d")
get_time = lambda: time.strftime("%H:%M:%S")


def shutdown_switch():
    input("press enter to shutdown the server\n")
    server.close()
    sys.exit()
    

def client(conn: soc.socket, addr: tuple):
    """Handles client requests

    Args:
        conn (soc.socket): socket connection object
        addr (tuple): (ip_4, port number)
    """
    db_conn = mysql.connector.connect(
        user=settings["database.client.user"],
        password=settings["database.client.password"],
        host=settings["database.host"],
        port=settings["database.port"],
        database=settings["database.schema"],
    )
    
    
    try:
        print("Connected to client on %s:%s" % addr)
        conn.send(f"Connected to {settings["server.host"]}:{settings["server.port"]}".encode())
    
    
        while True:
            message = json.loads(conn.recv(settings["general.buffer_size"]).decode())            
            flag: str = message['flag']
            text: str = message['text']
            print("%s:%s"% addr, get_time(), "-" , flag, text)
            
            cur = db_conn.cursor()            
            if flag == "CLOSE":
                print("Client on %s:%s has closed the connection"% addr)
                db_conn.close()
                conn.close()
                break
            
            if flag == "is_existing_customer":
                cur.execute("""
                            SELECT * FROM customers
                            WHERE phone = '%s';"""% text)
                
                answers = bool(cur.fetchall())
                if answers:
                    conn.send(f"True".encode())
                else:
                    conn.send(f"False".encode())
            
            elif flag == "Register Customer":
                try:
                    cur.execute("""
                                INSERT INTO customers(fname, sname, address, phone)
                                VALUES ('%s', '%s', '%s', '%s');
                                """% tuple(text.split(", ")))
                except:
                    print(sys.exc_info())
                    conn.send("False".encode())
                    cur.close()
                    continue
                
                conn.send("True".encode())
            
            elif flag == "Register Movie":
                
                try:
                    name, type_var = text.split(', ')
                
                    cur.execute("""
                                CALL sp_add_movie('%s', '%s', '%s');
                                """% (name.lower(), type_var.upper(), get_date()))
                except:
                    print(sys.exc_info())
                    conn.send("False".encode())
                    cur.close()
                    continue
                
                conn.send("True".encode())
                
            elif flag == "Hire Out Movie":
                
                try:
                    custID, videoID = text.split(", ")
                    
                    cur.execute("""
                                INSERT INTO hire(custId, videoId, dateHired)
                                VALUES (%s, %s, '%s');
                                """% (custID, videoID, get_date()))
                except:
                    print(sys.exc_info())
                    conn.send("False".encode())
                    cur.close()
                    continue
                
                conn.send("True".encode())
                
            elif flag == "Return Movie":
                
                try:
                    custID, videoID = text.split(", ")
                    
                    cur.execute("""
                                UPDATE hire
                                SET dateReturn = '%s'
                                WHERE custId = %s AND videoId = %s;
                                """% (get_date(), custID, videoID))
                except:
                    print(sys.exc_info())
                    conn.send("False".encode())
                    cur.close()
                    continue
                
                conn.send("True".encode())
                
            elif flag == "Find Customer ID":
                cur.execute("""
                            SELECT custId
                            FROM customers
                            WHERE phone = %s;
                            """% text)
                
                try:
                    answers = cur.fetchall()[0][0]
                except IndexError as ex:
                    conn.send(f"There is no account registered to that number".encode())
                    cur.close()
                    continue
                if answers:
                    conn.send(f"Your ID is: {answers}".encode())
                else:
                    conn.send("There is no customer registered to that phone number".encode())
                    
            elif flag == "Find Movie ID":
                cur.execute("""
                            SELECT videoId
                            FROM videos
                            WHERE vname = '%s';
                            """% text.lower())
                try:
                    answers = cur.fetchall()[0][0]
                except IndexError as ex:
                    conn.send(f"There is no move with that name".encode())
                    cur.close()
                    continue
                if answers:
                    conn.send(f"The Movies ID is: {answers}".encode())
                else:
                    conn.send("There is no movie registered under that name".encode())
                    
            elif flag == "Find Hired movies":
                try:
                    cur.execute('''
                                SELECT customers.fname, customers.sname, videos.vname, videos.videoId, hire.dateHired
                                FROM hire
                                LEFT JOIN customers
                                ON hire.custId = customers.custId
                                LEFT JOIN videos
                                ON hire.videoId = videos.videoId
                                WHERE hire.dateReturn IS NULL AND customers.phone = '%s';
                                '''% text)
                    
                    answers = cur.fetchall()
                    
                    return_list = []
                    for answer in answers:
                        name, surname, movie, videoID, dateHired = answer
                        return_list.append([name, surname, movie, str(videoID), str(dateHired)])
                        
                        
                    j_return = json.dumps(return_list)
                    conn.send(j_return.encode())
                    
                except:
                    print(sys.exc_info())
                    conn.send("[('Something went wrong')]".encode())
                    
                
            
            else:
                print("UNHANDLED FLAG!!!!")
                
                
                    
            cur.close()
            db_conn.commit()
                    
    except ConnectionResetError as ex:
        print("Client on %s:%s has closed the connection"% addr)
        
    finally:
        ex_info = sys.exc_info()
        if ex_info != (None, None, None):
            print(ex_info)
                
    
    db_conn.close()
    conn.close()


class Server:
    def __init__(self, ip, port) -> None:
        self.ip = ip
        self.port = port
        self.soc = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
        self.buffer_size = settings["general.buffer_size"]
        self.running = False

    def start(self):
        self.soc.bind((self.ip, self.port))
        self.soc.listen()
        
        self.running = True
        
        print("Server started of %s:%s" % (self.ip, self.port))

    def accept(self):
        conn, addr = self.soc.accept()

        thread = threading.Thread(target=client, args=(conn, addr))
        thread.start()
        
        return conn, addr

    def close(self):
        self.running = False
        self.soc.close()


try:
    if __name__ == "__main__":
        settings = {}
        with open("settings.json") as s:
            settings = json.loads(s.read())


        server = Server(settings["server.host"], settings["server.port"])
        server.start()
        
        threading.Thread(target=shutdown_switch).start()
        
        
        while server.running:
            try:
                server.accept()
            except OSError as ex:
                if ex.errno == 10038:
                    print("Good bye")
                    
                    
except FileNotFoundError as ex:
    print(ex)
    input("Press enter to close")
except:
    info = sys.exc_info()
    print(info)
    input("Press enter to close")
