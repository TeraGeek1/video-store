## client.py
import sys
import socket as soc
import os
import json


video_store_splash = (
    "=====================================================================\n"
    "|                            Video Store                            |\n"
    "=====================================================================\n"
)


class Client:
    def __init__(self, host, port) -> None:
        self.host = host
        self.port = port
        self.buffer_size = settings["general.buffer_size"]

        self.soc = soc.socket(soc.AF_INET, soc.SOCK_STREAM)

    def connect(self):
        self.soc.connect((self.host, self.port))
        print(self.recv())

    def recv(self):
        return self.soc.recv(self.buffer_size).decode()

    def send(self, flag, text: str = None):
        d_message = {"flag": flag, "text": text}
        j_message = json.dumps(d_message)
        self.soc.send(j_message.encode())

    def is_existing_customer(self, phone_number) -> bool:
        self.send("is_existing_customer", phone_number)
        return self.handle(self.recv())

    def register_customer(self, info) -> bool:
        self.send("Register Customer", info)
        return self.handle(self.recv())

    def register_movie(self, info) -> bool:
        client.send("Register Movie", info)
        return self.handle(self.recv())

    def hire_out_movie(self, info) -> bool:
        self.send("Hire Out Movie", info)
        return self.handle(self.recv())

    def return_movie(self, info) -> bool:
        self.send("Return Movie", info)
        return self.handle(self.recv())

    def find_customer_id(self, phone_number):
        self.send("Find Customer ID", phone_number)
        return self.handle(self.recv())

    def find_movie_id(self, movie_name):
        self.send("Find Movie ID", movie_name)
        return self.handle(self.recv())

    def find_hired_movies(self, phone_number):
        self.send("Find Hired movies", phone_number)
        movies: list = json.loads(self.recv())

        print("name, surname, movie, movie ID, dateHired")

        result = ""
        for movie in movies:
            movie: list
            result += ", ".join(movie) + "\n"

        return result

    def handle(self, val: str):
        if val == "True":
            return True
        elif val == "False":
            return False
        return val

    def close(self):
        self.send("CLOSE")
        self.soc.close()


def input_screen(function, screen_text, additional_agr=""):
    start_state = state
    while state == start_state:
        clear()
        print(video_store_splash + screen_text)
        info = input(":")
        if info == "":
            return states["Home Screen"]
        if function(info + additional_agr):
            return states["Home Screen"]
        else:
            print("Some thing went wrong try again")
            if input("Press enter to retry or q than enter to go to the home screen: "):
                return states["Home Screen"]
        clear()


def find_screen(function, screen_text):
    clear()
    print(video_store_splash + screen_text)
    print(function(input(":")))
    input("Press enter to go the the Home screen")
    return states["Home Screen"]


if __name__ == "__main__":
    clear = lambda: os.system("cls")

    settings = {}
    with open("settings.json") as s:
        settings = json.loads(s.read())

    client = Client(settings["server.host"], settings["server.port"])
    try:
        client.connect()
    except ConnectionRefusedError as ex:
        print(sys.exc_info())
        print("Could not connect to the server")
        input("press enter to kill the program")
        sys.exit()

    states = {
        "Home Screen": 0,
        "Register Customer": 1,
        "Register Movie": 2,
        "Hire Out Movie": 3,
        "Return Movie": 4,
        "Get Phone Num": 5,
        "Find Customer ID": 6,
        "Find Movie ID": 7,
        "Find Hired movies": 8,
    }

    state = states["Home Screen"]

    while True:
        if state == states["Home Screen"]:
            clear()
            print(
                "=====================================================================\n"
                "|                            Video Store                            |\n"
                "=====================================================================\n"
                "|  1. Register Customer                                             |\n"
                "|  2. Register Movie                                                |\n"
                "=====================================================================\n"
                "|  3. Hire Out Movie                                                |\n"
                "|  4. Return Movie                                                  |\n"
                "=====================================================================\n"
                "|  5. Find Customer ID                                              |\n"
                "|  6. Find Movie ID                                                 |\n"
                "|  7. Find Hired movies                                             |\n"
                "=====================================================================\n"
                "|  X. Exit                                                          |\n"
                "=====================================================================\n"
            )

            choice = input("Choice: ")

            if choice.lower() == "x":
                break

            if choice == "1":
                state = states["Get Phone Num"]
            elif choice == "2":
                state = states["Register Movie"]
            elif choice == "3":
                state = states["Hire Out Movie"]
            elif choice == "4":
                state = states["Return Movie"]
            elif choice == "5":
                state = states["Find Customer ID"]
            elif choice == "6":
                state = states["Find Movie ID"]
            elif choice == "7":
                state = states["Find Hired movies"]

        if state == states["Get Phone Num"]:
            clear()
            print(
                "=====================================================================\n"
                "|                            Video Store                            |\n"
                "=====================================================================\n"
                "| Please provide your phone number:                                 |\n"
                "| No spaces, no spacial characters, 10 digits long                  |\n"
                "| Good Example: 0725683216                                          |\n"
                "| Bad Example: 072-568-3216, 072 568 3216                           |\n"
                "=====================================================================\n"
            )
            p_number = input("Phone Number: ")
            if p_number == "":
                state = states["Home Screen"]
                continue
            elif len(p_number) == 10:
                message = client.is_existing_customer(p_number)
            else:
                continue

            clear()
            print(message)
            if message:
                print(
                    "=====================================================================\n"
                    "|                            Video Store                            |\n"
                    "=====================================================================\n"
                    "| You already have an account                                       |\n"
                    "=====================================================================\n"
                )
                input("Press enter to continue")
                state = states["Home Screen"]
            else:
                state = states["Register Customer"]

        elif state == states["Register Customer"]:
            state = input_screen(
                client.register_customer,
                "|                           Create account                          |\n"
                "=====================================================================\n"
                f"|  Phone Num: {p_number}                                            |\n"
                "|  Name, Surname, Address                                           |\n"
                "=====================================================================\n",
                f", {p_number}",
            )

        elif state == states["Register Movie"]:
            state = input_screen(
                client.register_movie,
                "|                           Register Movie                          |\n"
                "=====================================================================\n"
                "|  Title, type                                                      |\n"
                "=====================================================================\n",
            )

        elif state == states["Hire Out Movie"]:
            state = input_screen(
                client.hire_out_movie,
                "|                           Hire Out Movie                          |\n"
                "=====================================================================\n"
                "|  customer ID, video ID                                            |\n"
                "=====================================================================\n",
            )

        elif state == states["Return Movie"]:
            state = input_screen(
                client.return_movie,
                "|                            Return Movie                           |\n"
                "=====================================================================\n"
                "|  customer ID, video ID                                            |\n"
                "=====================================================================\n",
            )
        elif state == states["Find Customer ID"]:
            state = find_screen(
                client.find_customer_id,
                "|                          Find Customer ID                         |\n"
                "=====================================================================\n"
                "| Please provide your phone number:                                 |\n"
                "| No spaces, no spacial characters, 10 digits long                  |\n"
                "| Good Example: 0725683216                                          |\n"
                "| Bad Example: 072-568-3216, 072 568 3216                           |\n"
                "=====================================================================\n",
            )

        elif state == states["Find Movie ID"]:
            state = find_screen(
                client.find_movie_id,
                "|                          Find Customer ID                         |\n"
                "=====================================================================\n"
                "| Please enter the movies name                                      |\n"
                "=====================================================================\n",
            )

        elif state == states["Find Hired movies"]:
            state = find_screen(
                client.find_hired_movies,
                "|                          Find Hired movies                        |\n"
                "=====================================================================\n"
                "| Please provide your phone number:                                 |\n"
                "| No spaces, no spacial characters, 10 digits long                  |\n"
                "| Good Example: 0725683216                                          |\n"
                "| Bad Example: 072-568-3216, 072 568 3216                           |\n"
                "=====================================================================\n",
            )
    client.close()
