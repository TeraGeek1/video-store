import os
import sys


def pip_install(cmd: str, name):
    try:
        print()
        print(f"Installing {name}")
        os.system("python -m pip " + cmd)
    except:
        print(sys.exc_info())


pip_install(r"install pip", "pip")
pip_install(r"install mysql-connector-python==8.1.0", "MySQL Connector")
